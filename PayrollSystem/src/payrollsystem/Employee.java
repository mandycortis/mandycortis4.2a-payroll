package payrollsystem;

import java.io.Serializable;

public class Employee implements Serializable{
   
    private Integer ID;
    private String name;
    private String surname;
    private String contact;
    private String address;
    private String DOB;
    private String Gender;
    private String DateHired;
    private String Status;
    private String Job;
    private String type;
    private String Salary;
      
    public Employee () {
    }
    
    public Employee (Integer i, String n, String s, String c, String a, String d, String g, String dh, String st, String j, String t, String sl){
        ID = i;
        name = n;
        surname = s;
        contact = c;
        address = a;
        DOB = d;
        Gender = g;
        DateHired = dh;
        Status = st;
        Job = j;
        type = t;
        Salary = sl;
               
     }
    
    //getters to get data out of object
   public Integer getID() {
        return ID;
   }

    public String getName()  {
        return name;
    }
    
    public String getSurname() {
        return surname;
    }
    public String getContact() {
        return contact;
    }
    public String getAddress () {
        return address;
    }
    
    public String getDOB() {
        return DOB;
    }
    
    public String getGender() {
        return Gender;
    }
    
    public String getDateHired() {
        return DateHired;
    }
    
    public String getStatus() {
        return Status;
    }
    
    public String getJob() {
        return Job;
    }
    
    public String getType() {
        return type;
    }
    
    public String getSalary() {
        return Salary;
    }
    
   
    //setters to set values to object
    public void setID (Integer i){
        ID = i;
    }
    public void setName(String n){
        name = n;
    }
    public void setSurname(String s){
       surname = s;
    }
    public void setAddress(String a){
        address = a;
    }
    public void setContact(String c){
        contact = c;
    }
    public void setDOB(String d) {
        DOB = d;
    }
    
    public void setGender(String g) {
         Gender = g;
    }
    
    public void setDateHired(String dh) {
         DateHired = dh;
    }
    
    public void setStatus(String st) {
         Status = st;
    }
    
    public void setJob(String j) {
         Job = j;
    }
    
    public void setType(String t) {
         type = t;
    }
    
    public void setSalary(String sl) {
         Salary = sl;
    }

    
    
    public String toString() {
        return ID.toString() + " " + name + " " + surname + " " + contact +" "+ address +" "+ DOB +" "+ Gender +" "+ DateHired +" "+ Status +" "+ Job +" "+ type +" "+ Salary.toString();
    }
}



