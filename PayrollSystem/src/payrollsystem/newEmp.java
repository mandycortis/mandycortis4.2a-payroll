package payrollsystem;

import java.awt.*;
import javax.swing.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class newEmp extends javax.swing.JFrame {
    Connection conn=null;
    ResultSet rs=null;
    PreparedStatement pst=null;
    String connectionUrl =
                    "jdbc:sqlserver://MandyCortis:1433;"
                        + "database=Payroll;"
                        + "user=Mandy;"
                        + "password=Mandy;"
                        + "loginTimeout=30;";
    
    private ImageIcon format =null;
    String filename = null;
    byte[] personImg = null;
    String gender;
    String type;
    
    public newEmp() {
        initComponents();
        validate();
        Toolkit toolkit = getToolkit();
        Dimension size = toolkit.getScreenSize();
        setLocation(size.width/2 - getWidth()/2, 
        size.height/2 - getHeight()/2);
    }
    
    public boolean allLetters(String s) {
        //this method checks whether all characters inputted are letters
        for (int i = 0; i< s.length(); i++) {
            if(!Character.isLetter(s.charAt(i)))
                return false;
        }
        return true;
    }

    public boolean allDigits(String s){
        //this method checks whether all characters inputted are numbers
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isDigit(s.charAt(i)))
                return false;
        }
        return true;
    }
    
    private boolean validateName() {
        //calling the allLetters method to validate input and checking whether field is left empty
        try {
            if (!allLetters(txtName.getText())) {
                txtName.setText("Must contain only letters");
            }
            else if (txtName.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Name field must be filled");
                return false;
            }
            else {
                return true;
            }
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Invalid input in Name Field");
        }
        return false;
    }
    
    private boolean validateSurname() {
        try {
            if (!allLetters(txtSurname.getText())) {
                txtSurname.setText("Must contain only letters");
            }
            else if (txtSurname.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Surname field must be filled");
                return false;
            }
            else {
                return true;
            }
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Invalid input in Surname Field");
        }
        return false;
    }
    
    private boolean validateContact() {
        //calling the allDigits method to validate input and checking whether field is left empty
        try {
            if (!allDigits(txtContact.getText())) {
                txtContact.setText("Must be numeric");
                return false;
            }
            else if (txtContact.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Contact field must be filled");
                return false;
            }
            else if (txtContact.getText().length() < 8) {
                txtContact.setText("Number is too short");
                return false;
            }
        else {
            return true;
        
            }
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Invalid input in Contact Field");  
        } 
        return false;
    }
    
    private boolean validateSalary() {
        //calling the allDigits method to validate input and checking whether field is left empty
        try {
            if (!allDigits(txtSalary.getText())) {
                txtSalary.setText("Must be numeric");
                return false;
            }
            else if (txtSalary.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Salary field must be filled");
                return false;
            }
            else {
                return true;
            }
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Invalid input in Contact Field");  
        } 
        return false;
    }
    
    
    private boolean validateEmail(String email) {
        //calling the allDigits method to validate input and checking whether field is left empty
        try{
            String ePattern = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
            Pattern p = Pattern.compile(ePattern,Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(email);
            return m.find();
        }catch(Exception ex) {
            JOptionPane.showMessageDialog(null, ex);  
        } 
        return false;
    }
    
    private boolean valEmail(){
        String em = txtEmail.getText();
        
        try{
            if (!validateEmail(em)){
                txtEmail.setText("--@--.com");
                return false;
            }
            else{
                return true;
            }
        }catch  (Exception ex) {
            JOptionPane.showMessageDialog(null, "Invalid input in Email Field");  
        } 
        return false;
        }
    
        
    public boolean validateInput() {
        String em = txtEmail.getText();
        //calling all methods for validating text fields
        if (validateName() && validateSurname() && validateContact() && validateSalary() && valEmail()) {
            return true;
        }
        else {
            return false;
        }
    }
    
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblID = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblSurname = new javax.swing.JLabel();
        txtSurname = new javax.swing.JTextField();
        lblDOB = new javax.swing.JLabel();
        lblGender = new javax.swing.JLabel();
        rMale = new javax.swing.JRadioButton();
        rFemale = new javax.swing.JRadioButton();
        lblEmail = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        lblContact = new javax.swing.JLabel();
        txtContact = new javax.swing.JTextField();
        lblAddress = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        lblJob = new javax.swing.JLabel();
        txtJob = new javax.swing.JTextField();
        lblHired = new javax.swing.JLabel();
        lblSalary = new javax.swing.JLabel();
        txtSalary = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        dskImg = new javax.swing.JDesktopPane();
        img = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnImg = new javax.swing.JButton();
        rHourly = new javax.swing.JRadioButton();
        rSalaried = new javax.swing.JRadioButton();
        dpHired = new org.jdesktop.swingx.JXDatePicker();
        dpDOB = new org.jdesktop.swingx.JXDatePicker();
        lblStatus = new javax.swing.JLabel();
        cbStatus = new javax.swing.JComboBox<>();
        lblType = new javax.swing.JLabel();
        txtHourly = new javax.swing.JTextField();
        txtSalaried = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtDep = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblID.setText("Employee ID");

        txtID.setEditable(false);
        txtID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIDActionPerformed(evt);
            }
        });

        lblName.setText("Name");

        txtName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNameActionPerformed(evt);
            }
        });

        lblSurname.setText("Surname");

        txtSurname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSurnameActionPerformed(evt);
            }
        });

        lblDOB.setText("DOB ");

        lblGender.setText("Gender");

        rMale.setText("Male");
        rMale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rMaleActionPerformed(evt);
            }
        });

        rFemale.setText("Female");
        rFemale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rFemaleActionPerformed(evt);
            }
        });

        lblEmail.setText("Email");

        lblContact.setText("Contact");

        lblAddress.setText("Address");

        lblJob.setText("Job Title");

        txtJob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtJobActionPerformed(evt);
            }
        });

        lblHired.setText("Date Hired");

        lblSalary.setText("Salary");

        txtSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSalaryActionPerformed(evt);
            }
        });

        btnAdd.setText("Add Record");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnReset.setText("Reset");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        dskImg.setLayer(img, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout dskImgLayout = new javax.swing.GroupLayout(dskImg);
        dskImg.setLayout(dskImgLayout);
        dskImgLayout.setHorizontalGroup(
            dskImgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dskImgLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(img, javax.swing.GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
                .addContainerGap())
        );
        dskImgLayout.setVerticalGroup(
            dskImgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dskImgLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(img, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Add Employee");

        btnImg.setText("Upload Image");
        btnImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImgActionPerformed(evt);
            }
        });

        rHourly.setText("Hourly");
        rHourly.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rHourlyActionPerformed(evt);
            }
        });

        rSalaried.setText("Salaried");
        rSalaried.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rSalariedActionPerformed(evt);
            }
        });

        dpDOB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dpDOBActionPerformed(evt);
            }
        });

        lblStatus.setText("Status");

        cbStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Part-Time", "Full-Time" }));

        lblType.setText("Type");

        txtHourly.setEditable(false);
        txtHourly.setEnabled(false);
        txtHourly.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHourlyActionPerformed(evt);
            }
        });

        txtSalaried.setEditable(false);
        txtSalaried.setEnabled(false);

        jLabel2.setText("Department");

        txtDep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDepActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(217, 217, 217))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblGender)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(lblID)
                                                    .addComponent(lblName)
                                                    .addComponent(lblDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(lblSurname, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(dpDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(txtSurname, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(txtID, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(lblStatus)
                                                    .addGroup(layout.createSequentialGroup()
                                                        .addComponent(rMale)
                                                        .addGap(4, 4, 4)
                                                        .addComponent(rFemale)))
                                                .addGap(18, 18, 18)
                                                .addComponent(cbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(33, 33, 33)
                                        .addComponent(lblType)
                                        .addGap(13, 13, 13)))
                                .addComponent(btnImg))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(299, 299, 299)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSalary)
                                    .addComponent(lblHired)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(282, 282, 282)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(34, 34, 34)
                                        .addComponent(dpHired, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(109, 109, 109))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(27, 27, 27)
                                        .addComponent(lblJob)
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(txtHourly, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(rHourly))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(rSalaried)
                                                    .addComponent(txtSalaried, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(txtJob, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(1, 1, 1)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtDep, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(dskImg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(lblEmail)
                                        .addGap(32, 32, 32))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(lblContact)
                                        .addGap(18, 18, 18))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(lblAddress)
                                        .addGap(18, 18, 18)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtAddress, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtEmail, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtContact, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(55, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(lblID)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblName))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7)
                                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblSurname))))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDOB)
                            .addComponent(dpDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addComponent(lblGender)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rMale)
                            .addComponent(rFemale)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblEmail))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(txtContact, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lblContact))
                                    .addGap(95, 95, 95))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(50, 50, 50)
                                    .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(53, 53, 53)
                                    .addComponent(lblAddress)))
                            .addComponent(dskImg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnImg)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)))
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblJob)
                    .addComponent(txtJob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtDep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rHourly)
                            .addComponent(rSalaried)
                            .addComponent(lblStatus)
                            .addComponent(cbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblType))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtHourly, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSalaried, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblHired)
                            .addComponent(dpHired, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSalary))
                        .addGap(22, 22, 22))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNameActionPerformed

    private void txtSurnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSurnameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSurnameActionPerformed

    private void rMaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rMaleActionPerformed
        gender ="Male";
        rMale.setSelected(true);
        rFemale.setSelected(false);
    }//GEN-LAST:event_rMaleActionPerformed

    private void rFemaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rFemaleActionPerformed
        gender ="Female";
        rFemale.setSelected(true);
        rMale.setSelected(false);
    }//GEN-LAST:event_rFemaleActionPerformed

    private void txtJobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtJobActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtJobActionPerformed

    private void txtSalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSalaryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSalaryActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed

        int p = JOptionPane.showConfirmDialog(null, "Adding record. Confirm?","Add Record",JOptionPane.YES_NO_OPTION);
        if(p==0){
            try{
                if (!validateInput()) {
                JOptionPane.showMessageDialog(null, "Error in data input, please ammend");
                }else{
                conn = DriverManager.getConnection(connectionUrl);
                String query ="insert into Employee "
                + "(Name,Surname,Gender,DOB,"
                + "Status,Address,Email,"
                + "Mobile, DateHired, Salary, Job, Department, Type, Image) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                pst = conn.prepareStatement(query);
                
                
                pst.setString(1, txtName.getText());
                pst.setString(2, txtSurname.getText());               
                if(rMale.isSelected()) {
                    gender="Male";
                }
                if(rFemale.isSelected()) {
                    gender="Female";
                }
                pst.setString(3,gender);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String addDate = df.format(dpDOB.getDate());
                pst.setString(4,addDate);
                pst.setString(5,cbStatus.getSelectedItem().toString());
                pst.setString(6,txtAddress.getText());
                pst.setString(7,txtEmail.getText());
                pst.setString(8,txtContact.getText());
                String addDate2 = df.format(dpHired.getDate());
                pst.setString(9,addDate2);
                pst.setString(10,txtSalary.getText());
                pst.setString(11,txtJob.getText());
                pst.setString(12,txtJob.getText());
                
                
//                if(rHourly.isSelected()== true){
//                    double getH = Double.parseDouble(txtHourly.getText());
//                    String Perc = String.valueOf(getH);
//                    
//                    String sql = "insert into HourlyEmployee (HourlyRate, EmployeeID, Name, Surname) values (?,?,?,?)";
//                    pst = conn.prepareStatement(sql);
//                    pst.setString(1, Perc);
//                    pst.setString(2, txtID.getText());
//                    pst.setString(3, txtName.getText());
//                    pst.setString(4, txtSurname.getText());
//                    pst.execute();
//                    System.out.println("hourly");
//                }
//
//               else if(rSalaried.isSelected()==true){
//                    double getH1 = Double.parseDouble(txtHourly.getText());
//                    String Perc1 = String.valueOf(getH1);
//                    
//                    String sql = "insert into SalariedEmployee (Salary, EmployeeID, Name, Surname) values (?,?,?,?)";
//                    pst = conn.prepareStatement(sql);
//                    pst.setString(1, Perc1);
//                    pst.setString(2, txtID.getText());
//                    pst.setString(3, txtName.getText());
//                    pst.setString(4, txtSurname.getText());
//                    pst.execute();
//                    System.out.println("salary");
//               }
                
                if(rHourly.isSelected() == true) {
                    type="Hourly";
//                    String sql = "insert into HourlyEmployee (HourlyRate, EmployeeID, Name, Surname) values (?,?,?,?)";
//                    pst = conn.prepareStatement(sql);
//                    pst.setString(1, txtHourly.getText());
//                    pst.execute();
                }
                else if(rSalaried.isSelected() == true) {
                    type="Salaried";
//                    String sql1 = "insert into SalariedEmployee (Salary, EmployeeID, Name, Surname) values (?,?,?,?)";
//                    pst = conn.prepareStatement(sql1);
//                    pst.setString(2, txtSalaried.getText());
//                    pst.execute();
                }
                pst.setString(13,type);
                pst.setBytes(14,personImg);

                pst.executeUpdate();
                JOptionPane.showMessageDialog(null,"Record saved successfully!");

                }
            }catch(Exception e) {
                JOptionPane.showMessageDialog(null,e);
            }
        }  
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        txtID.setText("");
        txtName.setText("");
        txtSurname.setText("");
        txtContact.setText("");
        dpDOB.setDate(null);
        txtEmail.setText("");
        txtAddress.setText("");
        cbStatus.setSelectedIndex(0);
        txtSalary.setText("");
        txtJob.setText("");
        txtDep.setText("");
        dpHired.setDate(null);
        img.setIcon(null);
    }//GEN-LAST:event_btnResetActionPerformed

    private void btnImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImgActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        File f = chooser.getSelectedFile();
        
        filename =f.getAbsolutePath();
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(filename).getImage().getScaledInstance(img.getWidth(), img.getHeight(), Image.SCALE_DEFAULT));
        img.setIcon(imageIcon);
        try {
            File pic = new File(filename);
            FileInputStream fis = new FileInputStream (pic);
            ByteArrayOutputStream bos= new ByteArrayOutputStream();
            byte[] buf = new byte[1024];

            for(int readNum; (readNum=fis.read(buf))!=-1; ){
                bos.write(buf,0,readNum);
            }
            personImg=bos.toByteArray();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,e);
        }
    }//GEN-LAST:event_btnImgActionPerformed

    private void txtIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIDActionPerformed

    private void rHourlyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rHourlyActionPerformed
        type ="Hourly";
        rHourly.setSelected(true);
        rSalaried.setSelected(false);
        
        txtSalaried.setEnabled(false);
        txtHourly.setEditable(true);
        txtHourly.setEnabled(true);
        txtHourly.setText("");
    }//GEN-LAST:event_rHourlyActionPerformed

    private void rSalariedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSalariedActionPerformed
        type ="Salaried";
        rHourly.setSelected(false);
        rSalaried.setSelected(true);
        
        txtHourly.setEnabled(false);
        txtSalaried.setEditable(true);
        txtSalaried.setEnabled(true);
        txtSalaried.setText("");
    }//GEN-LAST:event_rSalariedActionPerformed

    private void dpDOBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dpDOBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dpDOBActionPerformed

    private void txtHourlyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHourlyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHourlyActionPerformed

    private void txtDepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDepActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDepActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new newEmp().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnImg;
    private javax.swing.JButton btnReset;
    private javax.swing.JComboBox<String> cbStatus;
    private org.jdesktop.swingx.JXDatePicker dpDOB;
    private org.jdesktop.swingx.JXDatePicker dpHired;
    private javax.swing.JDesktopPane dskImg;
    private javax.swing.JLabel img;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblContact;
    private javax.swing.JLabel lblDOB;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblGender;
    private javax.swing.JLabel lblHired;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblJob;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblSalary;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblSurname;
    private javax.swing.JLabel lblType;
    private javax.swing.JRadioButton rFemale;
    private javax.swing.JRadioButton rHourly;
    private javax.swing.JRadioButton rMale;
    private javax.swing.JRadioButton rSalaried;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtContact;
    private javax.swing.JTextField txtDep;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtHourly;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtJob;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtSalaried;
    private javax.swing.JTextField txtSalary;
    private javax.swing.JTextField txtSurname;
    // End of variables declaration//GEN-END:variables
}
