package payrollsystem;

import java.awt.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class Login extends javax.swing.JFrame {

    String connectionUrl =
                    "jdbc:sqlserver://MandyCortis:1433;"
                        + "database=Payroll;"
                        + "user=Mandy;"
                        + "password=Mandy;"
                        + "loginTimeout=30;";

    public Login() {
        initComponents();
        //conn = db.Load();
        Toolkit tk = getToolkit();
        Dimension dim = tk.getScreenSize();
        setLocation(dim.width/2 - getWidth()/2, dim.height/2 - getHeight()/2);
        //ResultSet rs = db.Load("SELECT * FROM [Login]");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblUser = new javax.swing.JLabel();
        lblPass = new javax.swing.JLabel();
        txtUser = new javax.swing.JTextField();
        txtPass = new javax.swing.JPasswordField();
        btnLogin = new javax.swing.JButton();
        lblUerr = new javax.swing.JLabel();
        lblPerr = new javax.swing.JLabel();
        lblUerr1 = new javax.swing.JLabel();
        cbUser = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();

        bg.setIcon(new javax.swing.ImageIcon("C:\\Users\\User\\Downloads\\abstract-1296710_1920.png")); // NOI18N
        bg.setText("jLabel1");

        jLabel1.setIcon(new javax.swing.ImageIcon("E:\\4.2A Second Semester\\Project\\Payroll\\PayrollSystem\\src\\resources\\login.jpg")); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        lblUser.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblUser.setText("Username");
        getContentPane().add(lblUser);
        lblUser.setBounds(410, 200, 90, 20);

        lblPass.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPass.setText("Password");
        getContentPane().add(lblPass);
        lblPass.setBounds(410, 260, 80, 20);

        txtUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUserActionPerformed(evt);
            }
        });
        getContentPane().add(txtUser);
        txtUser.setBounds(500, 200, 141, 20);

        txtPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPassActionPerformed(evt);
            }
        });
        getContentPane().add(txtPass);
        txtPass.setBounds(500, 260, 141, 20);

        btnLogin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        getContentPane().add(btnLogin);
        btnLogin.setBounds(530, 340, 90, 30);
        getContentPane().add(lblUerr);
        lblUerr.setBounds(170, 110, 120, 0);
        getContentPane().add(lblPerr);
        lblPerr.setBounds(160, 170, 130, 20);
        getContentPane().add(lblUerr1);
        lblUerr1.setBounds(160, 110, 140, 0);

        cbUser.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Admin", "Staff" }));
        getContentPane().add(cbUser);
        cbUser.setBounds(540, 300, 70, 20);

        jLabel2.setIcon(new javax.swing.ImageIcon("E:\\4.2A Second Semester\\Project\\Payroll\\PayrollSystem\\src\\resources\\login.jpg")); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 740, 510);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUserActionPerformed
        
    }//GEN-LAST:event_txtUserActionPerformed

    private boolean validateUsername() {
        if(txtUser.getText().isEmpty()) {
           JOptionPane.showMessageDialog(null, "Username must be filled");     
        }
        return false;
    }
    
    private boolean validatePassword() {
        if(txtPass.getPassword().length == 0){
            JOptionPane.showMessageDialog(null, "Password must be filled");  
        }
        return false;
    }
    
    private boolean validateAll() {
        if(validateUsername() && validatePassword()){
            JOptionPane.showMessageDialog(null, "Fields must be filled");  
        }
        return false;
    }
    
    private boolean validateInput() {
        if (validateUsername() && validatePassword() && validateAll()){
            return true;
        }
        else{
            return false;
        }
    }
    
    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        
        validateInput(); 
        
        try{
               Connection conn = DriverManager.getConnection(connectionUrl);
             
                String pass = new String(txtPass.getPassword());
                String query = "Select * from Login where Username=? and Password=? and Role=?";

         
               PreparedStatement pst=conn.prepareStatement(query);

               pst.setString(1,txtUser.getText());
               pst.setString(2,pass);
               pst.setString(3,cbUser.getSelectedItem().toString());

               ResultSet rs=pst.executeQuery();

               if(rs.next()){
                   JOptionPane.showMessageDialog(null, "Login Successfully");
                   PayrollMenu pm = new PayrollMenu();
                   pm.setVisible(true);
                   this.dispose();
                   int value = Emp.empId;
                   pst.execute();
               }else{
                   JOptionPane.showMessageDialog(null, "Login Unsuccessfully");
                   txtUser.setText("");
                   txtPass.setText("");
               }
               conn.close();
        }catch(Exception e) {
               JOptionPane.showMessageDialog(null, e);        
        }
    }//GEN-LAST:event_btnLoginActionPerformed
    
    private void txtPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPassActionPerformed
        
    }//GEN-LAST:event_txtPassActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bg;
    private javax.swing.JButton btnLogin;
    private javax.swing.JComboBox<String> cbUser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblPass;
    private javax.swing.JLabel lblPerr;
    private javax.swing.JLabel lblUerr;
    private javax.swing.JLabel lblUerr1;
    private javax.swing.JLabel lblUser;
    private javax.swing.JPasswordField txtPass;
    private javax.swing.JTextField txtUser;
    // End of variables declaration//GEN-END:variables
}
