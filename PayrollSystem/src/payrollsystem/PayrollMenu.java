package payrollsystem;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.*;
import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PayrollMenu extends javax.swing.JFrame {

    Connection conn=null;
    ResultSet rs=null;
    PreparedStatement pst=null;

    public PayrollMenu() {
        initComponents();
        ResultSet rs = db.Load("SELECT TOP 5 ID, DeptName FROM [Department]");
        Toolkit tk = getToolkit();
        Dimension dim = tk.getScreenSize();
        setLocation(dim.width/2 - getWidth()/2, dim.height/2 - getHeight()/2);
        txtEmpID.setText(String.valueOf(Emp.empId).toString());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        btnAllowances = new javax.swing.JButton();
        btnNewEmp = new javax.swing.JButton();
        btnDeductions = new javax.swing.JButton();
        btnSalary = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblEmp = new javax.swing.JLabel();
        txtEmpID = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menFile = new javax.swing.JMenu();
        menAdd = new javax.swing.JMenuItem();
        menPS = new javax.swing.JMenuItem();

        jMenu1.setText("File");
        jMenuBar2.add(jMenu1);

        jMenu3.setText("Edit");
        jMenuBar2.add(jMenu3);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        btnAllowances.setText("Allowances");
        btnAllowances.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllowancesActionPerformed(evt);
            }
        });
        getContentPane().add(btnAllowances);
        btnAllowances.setBounds(410, 240, 160, 70);

        btnNewEmp.setText("Add New Employees");
        btnNewEmp.setMaximumSize(new java.awt.Dimension(100, 20));
        btnNewEmp.setMinimumSize(new java.awt.Dimension(100, 20));
        btnNewEmp.setPreferredSize(new java.awt.Dimension(100, 20));
        btnNewEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewEmpActionPerformed(evt);
            }
        });
        getContentPane().add(btnNewEmp);
        btnNewEmp.setBounds(220, 140, 160, 70);

        btnDeductions.setText("Deductions");
        btnDeductions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeductionsActionPerformed(evt);
            }
        });
        getContentPane().add(btnDeductions);
        btnDeductions.setBounds(220, 240, 160, 70);

        btnSalary.setText("Update Salary");
        btnSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalaryActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalary);
        btnSalary.setBounds(320, 340, 160, 70);

        btnSearch.setText("Search Employees");
        btnSearch.setMaximumSize(new java.awt.Dimension(100, 20));
        btnSearch.setMinimumSize(new java.awt.Dimension(100, 20));
        btnSearch.setPreferredSize(new java.awt.Dimension(100, 20));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        getContentPane().add(btnSearch);
        btnSearch.setBounds(410, 140, 160, 70);

        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });
        getContentPane().add(btnLogout);
        btnLogout.setBounds(860, 50, 140, 30);

        jLabel1.setText("Logged in as:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(840, 10, 90, 14);
        getContentPane().add(lblEmp);
        lblEmp.setBounds(940, 10, 70, 0);

        txtEmpID.setText("ID");
        getContentPane().add(txtEmpID);
        txtEmpID.setBounds(930, 10, 100, 20);

        jLabel2.setIcon(new javax.swing.ImageIcon("E:\\4.2A Second Semester\\Project\\Payroll\\PayrollSystem\\src\\resources\\nature.jpeg")); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 1920, 650);

        jMenuBar1.setToolTipText("");
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        menFile.setText("File");

        menAdd.setText("Add user account");
        menAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menAddActionPerformed(evt);
            }
        });
        menFile.add(menAdd);

        menPS.setText("Generate PaySlip");
        menPS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menPSActionPerformed(evt);
            }
        });
        menFile.add(menPS);

        jMenuBar1.add(menFile);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAllowancesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllowancesActionPerformed
        Allowances alw = new Allowances();
        alw.setVisible(true);
    }//GEN-LAST:event_btnAllowancesActionPerformed

    private void btnNewEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewEmpActionPerformed
        newEmp ne = new newEmp();
        ne.setVisible(true);
    }//GEN-LAST:event_btnNewEmpActionPerformed

    private void btnDeductionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeductionsActionPerformed
        Deductions ded = new Deductions();
        ded.setVisible(true);
    }//GEN-LAST:event_btnDeductionsActionPerformed

    private void btnSalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalaryActionPerformed
        Salary sal = new Salary();
        sal.setVisible(true);
    }//GEN-LAST:event_btnSalaryActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        SearchEmp src = new SearchEmp();
        src.setVisible(true);
    }//GEN-LAST:event_btnSearchActionPerformed

    private void menAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menAddActionPerformed
        Account acc = new Account();
        acc.setVisible(true);
    }//GEN-LAST:event_menAddActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        Login lg = new Login();
        lg.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void menPSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menPSActionPerformed
        PaySlip ps = new PaySlip();
        ps.setVisible(true);
    }//GEN-LAST:event_menPSActionPerformed

    public static void main(String args[]) {
 
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PayrollMenu().setVisible(true);
            }
        });
    }   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAllowances;
    private javax.swing.JButton btnDeductions;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnNewEmp;
    private javax.swing.JButton btnSalary;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JLabel lblEmp;
    private javax.swing.JMenuItem menAdd;
    private javax.swing.JMenu menFile;
    private javax.swing.JMenuItem menPS;
    private javax.swing.JTextField txtEmpID;
    // End of variables declaration//GEN-END:variables
}
