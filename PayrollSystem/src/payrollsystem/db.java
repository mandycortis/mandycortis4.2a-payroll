package payrollsystem;

import java.awt.List;
import java.sql.*;
import java.util.ArrayList;
import javafx.scene.chart.PieChart.Data;

public class db {
    
//    public static final String TBL_ALLOWANCES = "Allowances";
//    public static final String TBL_DEDUCTIONS = "Deductions";
//    
//    
    
//    public static void Load() {
//        String connectionUrl =
//
//                 "jdbc:sqlserver://MandyCortis:1433;"
//
//                        + "database=Payroll;"
//               
//                        + "user=Mandy;"
//
//                        + "password=Mandy;"
//
//                        + "loginTimeout=30;";
//    }
//        
//    public static void InsertEmployee(newEmp Employee) {
//        String connectionUrl =
//
//                 "jdbc:sqlserver://MandyCortis:1433;"
//
//                        + "database=Payroll;"
//               
//                        + "user=Mandy;"
//
//                        + "password=Mandy;"
//
//                        + "loginTimeout=30;";
//
// 
//        ResultSet rs = null;
//
//        try (Connection conn = DriverManager.getConnection(connectionUrl);
//
//            Statement st = conn.createStatement();) {
//            String query ="insert into Employee "
//                + "(Name,Surname,Gender,DOB,"
//                + "Status,Address,Email,"
//                + "Mobile, DateHired, Salary, Job, Type) values (" + EmpName + "'" +
//            ,?,?,?,?,?,?,?,?,?,?,?)";
//            
//            
//            st.executeUpdate(query); 
//        }
//        catch (SQLException e) {
//            e.printStackTrace();
//        }
//        }
//        
//        
//        
//        
//        try { 
//            String url = "jdbc:msql://200.210.220.1:1114/Demo"; 
//            Connection conn = DriverManager.getConnection(url,"",""); 
//            Statement st = conn.createStatement(); 
//            st.executeUpdate("INSERT INTO Customers " + 
//                "VALUES (1001, 'Simpson', 'Mr.', 'Springfield', 2001)"); 
//            st.executeUpdate("INSERT INTO Customers " + 
//                "VALUES (1002, 'McBeal', 'Ms.', 'Boston', 2004)"); 
//            st.executeUpdate("INSERT INTO Customers " + 
//                "VALUES (1003, 'Flinstone', 'Mr.', 'Bedrock', 2003)"); 
//            st.executeUpdate("INSERT INTO Customers " + 
//                "VALUES (1004, 'Cramden', 'Mr.', 'New York', 2001)");
//
//            conn.close(); 
//        } catch (Exception e) { 
//            System.err.println("Got an exception! "); 
//            System.err.println(e.getMessage()); 
//        } 
//    }
//
//    public void DisplayResults()
//    {
//        ResultSet depts = Load("SELECT * FROM Department");
//        try
//        {
//        while (depts.next()) {
//
//                System.out.println(depts.getString(1) + " " + depts.getString(2));
//
//            }
//        }
//        catch (SQLException e) {
//            e.printStackTrace();
//        }
//        
//        int empNum = 2;
//        ResultSet emps = Load("SELECT * FROM Employee WHERE ID = " + empNum);
//        try
//        {
//        while (emps.next()) {
//
//                System.out.println(emps.getString(1) + " " + emps.getString(2));
//
//            }
//        }
//        catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
    
    public static ResultSet Load(String command){

        String connectionUrl =
                    "jdbc:sqlserver://MandyCortis:1433;"
                        + "database=Payroll;"
                        + "user=Mandy;"
                        + "password=Mandy;"
                        + "loginTimeout=30;";

        ResultSet rs = null;

        try (Connection conn = DriverManager.getConnection(connectionUrl);

            Statement statement = conn.createStatement();) {

        String selectSql = command;
        rs = statement.executeQuery(selectSql);

        return rs;

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
     return null;
    }
}